## Task: Build Server component with Mass Transit

- [ ] Configure MassTransit
```csharp
builder.Services.AddMassTransit(x =>
{
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host("localhost", "/", h =>
        {
            h.Username("rabbitmq");
            h.Password("rabbitmq");
        });
    });
});
```
- [ ] Add endpoint which will be triggering request / response flow
```csharp
app.MapPost("/request-bus-times", async (IBus bus, [FromBody]CheckBusTime request) =>
{
    var client = bus.CreateRequestClient<CheckBusTime>();
    var response = await client.GetResponse<BusTimeResponse>(request);
    return Results.Ok(response.Message);
});
```