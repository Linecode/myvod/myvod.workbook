﻿namespace Async2.Contracts;

public class ValueEntered
{
    public string Value { get; set; }
}