# Task: Create Http Polling Client, that works in background:

- [ ] Define class PollingScheduler
```csharp
public class PollingScheduler : List<Guid>;
```
- [ ] and Add it to DI container as a Singleton
- [ ] Define Hosted service
```csharp
public class Service(IServiceProvider serviceProvider) : BackgroundService
{
    static HttpClient _httpClient = new();
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(3000, stoppingToken);
            
            using var scope = serviceProvider.CreateScope();
            var polls = scope.ServiceProvider.GetRequiredService<PollingScheduler>();
            
            var listToRemove = new List<Guid>();
            
            foreach (var poll in polls)
            {
                Console.WriteLine($"Polling job {poll}");
                var response = await _httpClient.GetAsync($"http://localhost:5284/job/{poll}", stoppingToken);
                
                var responseContent = await response.Content.ReadAsStringAsync(stoppingToken);
                var value = Convert.ToInt32(responseContent);

                if (value == 1)
                {
                    listToRemove.Add(poll);
                    Console.WriteLine("Yay! Job completed!");
                }
            }
            
            foreach (var poll in listToRemove)
            {
                polls.Remove(poll);
            }
        }
    }
}
```
- [ ] Add it to DI container as a Hosted Service
- [ ] Create endpoint with HTTP Client usage to call `Async3HttpServer`
```csharp
var httpClient = new HttpClient();

app.MapPost("/trigger", async (PollingScheduler pollingScheduler) =>
{
    var response = await httpClient.PostAsync("http://localhost:5284/job", new StringContent(""));
    
    var guid = response.Headers.Location?.ToString().Split("/").Last() ?? string.Empty;
    
    if (!string.IsNullOrEmpty(guid))
        pollingScheduler.Add(Guid.Parse(guid));
    
    return Results.Accepted();
});
```