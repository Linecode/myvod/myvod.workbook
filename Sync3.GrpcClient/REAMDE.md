# Task: Define Endpoint with will use gRPC Client

- [ ] define endpoint that will use gRPC client to query data from `Sync.3.GrpcService`
```csharp
app.MapGet("/call-grpc-service", () =>
{
    using var channel = GrpcChannel.ForAddress("http://localhost:5151");
    var client = new MovieReviewService.MovieReviewServiceClient(channel);
    var response = client.GetReviewsForMovie(new GetReviewsForMovieRequest
    {
        MovieId = "1"
    });
    return response;
});
```