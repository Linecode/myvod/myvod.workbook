## Task: Create gRPC Server

- [ ] Define in memory repository
```csharp
public class ReviewsInMemoryRepository : List<MovieReview>
{
    public ReviewsInMemoryRepository()
    {
        AddRange(new List<MovieReview>()
        {
            new()
            {
                StarRating = 5, Comment = "Great movie!", Date = Timestamp.FromDateTime(DateTime.UtcNow),
                UserId = "user1"
            },
            new()
            {
                StarRating = 5, Comment = "Great movie!", Date = Timestamp.FromDateTime(DateTime.UtcNow),
                UserId = "user1"
            },
            new()
            {
                StarRating = 5, Comment = "Great movie!", Date = Timestamp.FromDateTime(DateTime.UtcNow),
                UserId = "user1"
            },
            new()
            {
                StarRating = 5, Comment = "Great movie!", Date = Timestamp.FromDateTime(DateTime.UtcNow),
                UserId = "user1"
            },
            new()
            {
                StarRating = 5, Comment = "Great movie!", Date = Timestamp.FromDateTime(DateTime.UtcNow),
                UserId = "user1"
            },
            new()
            {
                StarRating = 5, Comment = "Great movie!", Date = Timestamp.FromDateTime(DateTime.UtcNow),
                UserId = "user1"
            }
        });
    }
}
```
- [ ] Add repository to DI container as a singleton
- [ ] Define grpc service based on `vodreviews.proto` file
```csharp
public class MovieReviewsService(ILogger<MovieReviewsService> logger, ReviewsInMemoryRepository inMemoryRepository)
    : MovieReviewService.MovieReviewServiceBase
{
    private readonly ILogger<MovieReviewsService> _logger = logger;
    private readonly ReviewsInMemoryRepository _inMemoryRepository = inMemoryRepository;

    public override Task<GetReviewsForMovieResponse> GetReviewsForMovie(GetReviewsForMovieRequest request,
        ServerCallContext context)
    {
    }

    public override Task<CreateReviewResponse> CreateMovieReview(CreateReviewRequest request, ServerCallContext context)
    {
    }

    public override Task<GetUserReviewsResponse> GetUserReviews(GetUserReviewsRequest request,
        ServerCallContext context)
    {
    }
}
```
- [ ] To return value from this method you can wrap it into `Task.FromResult` like: 
```csharp

    public override Task<GetReviewsForMovieResponse> GetReviewsForMovie(GetReviewsForMovieRequest request,
        ServerCallContext context)
    {
        return Task.FromResult(new GetReviewsForMovieResponse
        {
            Reviews =
            {
                _inMemoryRepository.Where(x => true).ToList()
            }
        });
    }
```
- [ ] Add gRPC service to application builder with `app.MapGrpcService` method