## Task: Build GraphQL Server

- [ ] Define Model
```csharp
public class Book
{
    public Guid Id { get; set; }
    
    public string Title { get; set; }

    public Author Author { get; set; }
}

public class Author
{
    public string Name { get; set; }
}
```
- [ ] Define InMemoryRepository
```csharp
public class BooksRepository : List<Book>
{
    public BooksRepository()
    {
        Add(new Book
        {
            Id = new Guid("2A77FA51-838F-4EC4-B4B6-8E59ECC104B9"),
            Title = "Book 1",
            Author = new Author
            {
                Name = "John Doe"
            }
        });
    }
};
```
- [ ] Add INMemoryRepository to DI container
- [ ] Define Input and Payload data structures
```csharp
public record CreateBookInput(string Title, string AuthorName);
public record CreateBookPayload(Book Book);
```
- [ ] Define Query Type
```csharp
public class Query
{
    public IList<Book> GetBooks([Service] BooksRepository repository) 
        => repository.ToList();
    
    public Book GetBookById([Service] BooksRepository repository, [ID] Guid id) 
        => repository.FirstOrDefault(x => x.Id == id);
}
```
- [ ] define mutation type
```csharp
public class Mutation
{
    public CreateBookPayload CreateBook([Service] BooksRepository repository ,CreateBookInput input)
    {
        var book = new Book
        {
            Id = Guid.NewGuid(),
            Title = input.Title,
            Author = new Author
            {
                Name = input.AuthorName
            }
        };
        
        repository.Add(book);
        
        return new CreateBookPayload(book);
    }
}
```
- [ ] Add all in DI contaienr
```csharp
builder.Services.AddGraphQLServer()
    .AddQueryType<Query>()
    .AddMutationType<Mutation>()
    .AddDiagnosticEventListener(sp =>
        new ConsoleQueryLogger
            (sp.GetApplicationService<ILogger<ConsoleQueryLogger>>()));
```
You can also add ConsoleQueryLogger to display queries on Console.

Below there are some queries, so you could test the api. 

### Queries

#### GetAllBooks

```graphql
query GetAllBooks {
  books {
    id
    title
    author {
      name
    }
  }
}
```

#### GetBookById

```graphql
query GetBookById($id: ID!) {
  bookById(id: $id) {
    id
    title
    author {
      name
    }
  }
}
```
variables:

```json
{
  "id": "2a77fa51-838f-4ec4-b4b6-8e59ecc104b9"
}
```

#### AddBook

```graphql
mutation AddBook($input: CreateBookInput!) {
  createBook(input: $input) {
    book {
      id
      title
      author {
        name
      }
    }
  }
}
```

variables:
```json
{
  "input": {
      "title": "Example Book",
      "authorName": "Author Name"
    }
}
```
