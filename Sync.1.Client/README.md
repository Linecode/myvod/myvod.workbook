## Task: Create Http Client for Sync.1.Server

- [ ] Create Http client class
```csharp
public class ServerHttpClient(HttpClient httpClient)
{
    public async Task<WeatherForecast[]?> GetWeatherForecast()
    {
        var response = await httpClient.GetAsync("http://localhost:5028/weatherforecast");
        response.EnsureSuccessStatusCode();
        var content = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<WeatherForecast[]>(content);
    }
}
```
- [ ] Register it in DI container
```csharp
builder.Services.AddHttpClient<ServerHttpClient>();
```
- [ ] Create endpoint that will use HttpClient to call `Sync.1.Server` and return the response
```csharp
app.MapGet("/weatherforecast", async (ServerHttpClient httpClient) =>
    {
        var forecast = await httpClient.GetWeatherForecast();
        if (forecast is null) return Results.NotFound();
        return Results.Ok(forecast);
    })
    .WithName("GetWeatherForecast")
    .WithOpenApi();
```