## Task: Create WebApi Server that accepts webhooks as a for of communication

- [ ] Create Http Client for `Async1HttpWorker`
```csharp
public class AsyncHttpClient
{
    private readonly HttpClient _httpClient;
    
    public AsyncHttpClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
    
    public async Task<HttpResponseMessage> TriggerJob()
    {
        var response = await _httpClient.PostAsync("http://localhost:5141/job", new StringContent(""));
        return response;
    }
}
```
- [ ] Register this client into DI container with `builder.Services.AddHttpClient<AsyncHttpClient>();`
- [ ] Define store for JobStatus
```csharp
public class JobStatus
{
    public int Status { get; set; }
};
```
- [ ] Register it in DI container as Singleton 
- [ ] Define 3 endpoints
  - [ ] `POST: /start-job` for starting a job (Needs to trigger job endpoint with HTTP Client and change job status)
  - [ ] `GET: /job-status` will return job-status
  - [ ] `POST: /webhook` will change job status
```csharp
app.MapPost("/start-job", async ([FromServices]JobStatus jobStatus, [FromServices] AsyncHttpClient httpClient) =>
{
    var response = await httpClient.TriggerJob();
    
    if (response.StatusCode == HttpStatusCode.Accepted)
        jobStatus.Status = 1;
});

app.MapGet("/job-status", ([FromServices] JobStatus jobStatus) => jobStatus);

app.MapPost("/webhook", ([FromServices] JobStatus jobStatus) => jobStatus.Status = 2);
```
