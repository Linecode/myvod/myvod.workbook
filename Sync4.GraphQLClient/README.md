## Task: Create API that will fetch data from Sync4.GraphQL

Try it on your own based on this documentation: https://github.com/graphql-dotnet/graphql-client

In case of problem here is the whole process:

- Model: 
```csharp
public class Book
{
    public Guid Id { get; set; }
    
    public string Title { get; set; }

    public Author Author { get; set; }
}

public class Author
{
    public string Name { get; set; }
}

public record BooksResponse(List<Book> Books);
public record SingleBookResponse(Book BookById);
public record InsertBook(string Title, string AuthorName);
public record CreatedBookPayload(Book Book);
```
- Endpoints:
```csharp
app.MapGet("/books", async () =>
{
    using var graphQlClient = new GraphQLHttpClient("http://localhost:5069/graphql", new SystemTextJsonSerializer());

    var request = new GraphQLRequest
    {
        Query = """
                query GetAllBooks {
                  books {
                    id
                    title
                    author {
                      name
                    }
                  }
                }
                """
    };

    var response = await graphQlClient.SendQueryAsync<BooksResponse>(request);

    return response.Data;
});

app.MapGet("/book/{id}", async (Guid id) =>
{
    using var graphQlClient = new GraphQLHttpClient("http://localhost:5069/graphql", new SystemTextJsonSerializer());

    var request = new GraphQLRequest
    {
        Query = """
                query GetBookById($id: ID!) {
                  bookById(id: $id) {
                    id
                    title
                    author {
                      name
                    }
                  }
                }
                """,
        Variables = new
        {
            id = id.ToString()
        }
    };

    var response = await graphQlClient.SendQueryAsync<SingleBookResponse>(request);

    return response.Data;
});

app.MapPost("/book", async (InsertBook book) =>
{
    using var graphQlClient = new GraphQLHttpClient("http://localhost:5069/graphql", new SystemTextJsonSerializer());

    var request = new GraphQLRequest
    {
        Query = """
                mutation AddBook($input: CreateBookInput!) {
                  createBook(input: $input) {
                    book {
                      id
                      title
                      author {
                        name
                      }
                    }
                  }
                }
                """,
        Variables = new
        {
            input = new
            {
                book.Title, book.AuthorName
            }
        }
    };

    var response = await graphQlClient.SendQueryAsync<Book>(request);

    return response.Data;
});
```