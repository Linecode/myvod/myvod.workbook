## Task: Build a web server that can communicate over http polling

- [ ] Define jobScheduler class
```csahrp
public class JobsScheduler : Dictionary<Guid,int>;
```
- [ ] Add it to DI container as a Singleton
- [ ] Define JobHostedService
```csharp
public class JobsHostedService(IServiceProvider serviceProvider) : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(30000, stoppingToken);
            
            using var scope = serviceProvider.CreateScope();
            var jobsScheduler = scope.ServiceProvider.GetRequiredService<JobsScheduler>();
            
            if (jobsScheduler.Count > 0)
            {
                var job = jobsScheduler.FirstOrDefault(x => x.Value == 0);
                jobsScheduler[job.Key] = 1;
                Console.WriteLine("Job executed");
            }
        }
    }
}
```
- [ ] Add it to DI container as a Hosted Service
- [ ] Define the endpoint that will start an async job and will return job id
```csharp
app.MapPost("/job", (HttpContext context, JobsScheduler jobsScheduler) =>
{
    var id = Guid.NewGuid();
    var status = 0;
    jobsScheduler.Add(id, status);
    
    context.Response.Headers.Location = "/job/" + id;
    
    return Results.Accepted();
});
```
- [ ] Define the endpoint where we can ask if job already finished
```csharp
app.MapGet("/job/{id}",
    (HttpContext context,
        Guid id,
        JobsScheduler jobsScheduler) => !jobsScheduler.TryGetValue(id,
        out var value)
        ? Results.NotFound()
        : Results.Ok((object?)value));
```