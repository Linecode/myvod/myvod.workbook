## Task: Implement async sender with MassTransit Library

- [ ] Add Nuget package `MassTransit.RabbitMQ`
- [ ] Define connection with Message Broker
```csharp
var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
{
    cfg.Host("localhost", "/", h =>
    {
        h.Username("guest");
        h.Password("guest");
    });
});
```
if you are using dokcer-compsoe from MyVod project to run rabbitmq then credentials will be rabbitmq:rabbitmq
- [ ] Add `await busControl.StartAsync()` line to connect with message broker
- [ ] In bus configuration add endpoint definition
```csharp
cfg.ReceiveEndpoint("value-entered-queue", e =>
{
    e.Consumer<ValueEnteredConsumer>();
});
```
- Declare consumer
```csharp
public class ValueEnteredConsumer : IConsumer<ValueEntered>
{
    public Task Consume(ConsumeContext<ValueEntered> context)
    {
        Console.WriteLine($"Received: {context.Message.Value}");
        return Task.CompletedTask;
    }
}
```