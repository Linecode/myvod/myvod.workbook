namespace Sync5.Contracts;

public class CheckBusTime
{
    public string StopId { get; set; }
}

public class BusTimeResponse
{
    public string StopId { get; set; }
    public List<string> Times { get; set; }
}