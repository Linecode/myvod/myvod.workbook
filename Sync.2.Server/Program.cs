using Asp.Versioning;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
// builder.Services.AddEndpointsApiExplorer();
builder.Services.AddApiVersioning(opt =>
{
    opt.DefaultApiVersion = new ApiVersion(1, 0);
    opt.AssumeDefaultVersionWhenUnspecified = true;
    opt.ReportApiVersions = true;
    opt.ApiVersionReader = ApiVersionReader.Combine(
        new QueryStringApiVersionReader("api-version"),
        new HeaderApiVersionReader("api-version"),
        new UrlSegmentApiVersionReader());
});


var app = builder.Build();

app.UseHttpsRedirection();

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

var apiVersionSet = app.NewApiVersionSet()
    .HasApiVersion(new ApiVersion(1))
    .HasApiVersion(new ApiVersion(2))
    .ReportApiVersions()
    .Build();

app.MapGet("/v{version:apiVersion}/weatherforecast", () =>
    {
        Console.WriteLine("V1");
        var forecast = Enumerable.Range(1, 5).Select(index =>
                new V1.WeatherForecast
                (
                    DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    Random.Shared.Next(-20, 55),
                    summaries[Random.Shared.Next(summaries.Length)]
                ))
            .ToArray();
        return forecast;
    })
    .WithName("GetWeatherForecast Version 1")
    .WithApiVersionSet(apiVersionSet)
    .MapToApiVersion(1)
    .WithOpenApi();

app.MapGet("/v{version:apiVersion}/weatherforecast", () =>
    {
        Console.WriteLine("V2");
        var forecast = Enumerable.Range(1, 5).Select(index =>
                new V2.WeatherForecast
                (
                    DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    Random.Shared.Next(-20, 55),
                    summaries[Random.Shared.Next(summaries.Length)],
                    Random.Shared.Next(-20, 55)
                ))
            .ToArray();
        return forecast;
    })
    .WithName("GetWeatherForecast Version 2")
    .WithApiVersionSet(apiVersionSet)
    .MapToApiVersion(2)
    .WithOpenApi();

app.Run();


namespace V1
{
    public record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
    {
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
    }
}

namespace V2
{
    public record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary, int BreakingChange)
    {
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
    }
}
