## Task: Implement Versioning for Sync.2.Client

- [ ] Add nuget packages:
```csharp
        <PackageReference Include="Asp.Versioning.Http" Version="8.1.0" />
        <PackageReference Include="Asp.Versioning.Mvc.ApiExplorer" Version="8.1.0" />
```
- [ ] Add Api versioning, with configuration where server should look for version values
```csharp
builder.Services.AddApiVersioning(opt =>
{
    opt.DefaultApiVersion = new ApiVersion(1, 0);
    opt.AssumeDefaultVersionWhenUnspecified = true;
    opt.ReportApiVersions = true;
    opt.ApiVersionReader = ApiVersionReader.Combine(
        new QueryStringApiVersionReader("api-version"),
        new HeaderApiVersionReader("api-version"),
        new UrlSegmentApiVersionReader());
});
```
- [ ] Define API Version Set
```csharp
var apiVersionSet = app.NewApiVersionSet()
    .HasApiVersion(new ApiVersion(1))
    .HasApiVersion(new ApiVersion(2))
    .ReportApiVersions()
    .Build();
```
- [ ] Implement second `WeatherForecast` class, extend it with new property `int BreakingChange`
- [ ] Put both `WeatherForecast` classes into different namespaces (e.g. V1 and V2)
- [ ] Implement new endpoints: 
```csharp
app.MapGet("/v{version:apiVersion}/weatherforecast", () =>
    {
        Console.WriteLine("V1");
        var forecast = Enumerable.Range(1, 5).Select(index =>
                new V1.WeatherForecast
                (
                    DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    Random.Shared.Next(-20, 55),
                    summaries[Random.Shared.Next(summaries.Length)]
                ))
            .ToArray();
        return forecast;
    })
    .WithName("GetWeatherForecast Version 1")
    .WithApiVersionSet(apiVersionSet)
    .MapToApiVersion(1)
    .WithOpenApi();

app.MapGet("/v{version:apiVersion}/weatherforecast", () =>
    {
        Console.WriteLine("V2");
        var forecast = Enumerable.Range(1, 5).Select(index =>
                new V2.WeatherForecast
                (
                    DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    Random.Shared.Next(-20, 55),
                    summaries[Random.Shared.Next(summaries.Length)],
                    Random.Shared.Next(-20, 55)
                ))
            .ToArray();
        return forecast;
    })
    .WithName("GetWeatherForecast Version 2")
    .WithApiVersionSet(apiVersionSet)
    .MapToApiVersion(2)
    .WithOpenApi();
```