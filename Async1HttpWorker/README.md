## Task: Create WebApi Worker

- [ ] Create Job Scheduler (it can be as simple as some list of tasks - number)
```csharp
public class JobsScheduler : List<int>;
```
- [ ] Register it as a singleton
```csharp
builder.Services.AddSingleton<JobsScheduler>();
```
- [ ] Create an endpoint that will populate jobs scheduler
```csharp
app.MapPost("/job", (JobsScheduler jobsScheduler) =>
{
    jobsScheduler.Add(1);
    return Results.Accepted();
});
```
- [ ] Add an hostend / background service, which will process Jobs list in background, and call `Async1Http` when it will complete 
```csharp
public class JobsHostedService(IServiceProvider serviceProvider) : BackgroundService
{
    private static HttpClient _httpClient = new();
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(30000, stoppingToken);
            
            using var scope = serviceProvider.CreateScope();
            var jobsScheduler = scope.ServiceProvider.GetRequiredService<JobsScheduler>();
            
            if (jobsScheduler.Count > 0)
            {
                jobsScheduler.RemoveAt(0);
                Console.WriteLine("Job executed");

                await _httpClient.PostAsync("http://localhost:5067/webhook", new StringContent(""));
            }
        }
    }
}
```
- [ ] Register it in DI container as a HostedService
