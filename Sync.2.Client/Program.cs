using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpClient<ServerHttpClient>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/weatherforecast", async (ServerHttpClient httpClient) =>
    {
        var forecast = await httpClient.GetWeatherForecast();
        if (forecast is null) return Results.NotFound();
        return Results.Ok(forecast);
    })
    .WithName("GetWeatherForecast")
    .WithOpenApi();

app.Run();

public class ServerHttpClient(HttpClient httpClient)
{
    public async Task<WeatherForecast[]?> GetWeatherForecast()
    {
        var response = await httpClient.GetAsync("http://localhost:5058/v2/weatherforecast");
        response.EnsureSuccessStatusCode();
        var content = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<WeatherForecast[]>(content);
    }
}

public record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary, int BreakingChange)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}