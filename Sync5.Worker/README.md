## Task: Create Worker with MassTransit

- [ ] Configure MassTransit Library
```csharp
builder.Services.AddMassTransit(x =>
{
    x.AddConsumer<BusTimeConsumer>();
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host("localhost", "/", h =>
        {
            h.Username("rabbitmq");
            h.Password("rabbitmq");
        });

        cfg.ReceiveEndpoint("bus-time-service", e =>
        {
            e.ConfigureConsumer<BusTimeConsumer>(context);
        });
    });
});
```
- [ ] Add BusTimeConsumer
```csharp
public class BusTimeConsumer : IConsumer<CheckBusTime>
{
    public async Task Consume(ConsumeContext<CheckBusTime> context)
    {
        var times = new List<string> { "10:00 AM", "10:15 AM", "10:30 AM" }; // Simulated times
        await context.RespondAsync(new BusTimeResponse
        {
            StopId = context.Message.StopId,
            Times = times
        });
    }
}
```