var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<PollingScheduler>();
builder.Services.AddHostedService<Service>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

var httpClient = new HttpClient();

app.MapPost("/trigger", async (PollingScheduler pollingScheduler) =>
{
    var response = await httpClient.PostAsync("http://localhost:5112/job", new StringContent(""));
    
    var guid = response.Headers.Location?.ToString().Split("/").Last() ?? string.Empty;
    
    if (!string.IsNullOrEmpty(guid))
        pollingScheduler.Add(Guid.Parse(guid));
    
    return Results.Accepted();
});

app.Run();

public class PollingScheduler : List<Guid>;

public class Service(IServiceProvider serviceProvider) : BackgroundService
{
    static HttpClient _httpClient = new();
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(3000, stoppingToken);
            
            using var scope = serviceProvider.CreateScope();
            var polls = scope.ServiceProvider.GetRequiredService<PollingScheduler>();
            
            var listToRemove = new List<Guid>();
            
            foreach (var poll in polls)
            {
                Console.WriteLine($"Polling job {poll}");
                var response = await _httpClient.GetAsync($"http://localhost:5112/job/{poll}", stoppingToken);
                
                var responseContent = await response.Content.ReadAsStringAsync(stoppingToken);
                var value = Convert.ToInt32(responseContent);

                if (value == 1)
                {
                    listToRemove.Add(poll);
                    Console.WriteLine("Yay! Job completed!");
                }
            }
            
            foreach (var poll in listToRemove)
            {
                polls.Remove(poll);
            }
        }
    }
}