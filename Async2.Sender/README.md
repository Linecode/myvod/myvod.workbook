## Task: Implement async receiver with MassTransit Library

- [ ] Install the NuGet package MassTransit.RabbitMQ.
- [ ] Establish a connection with the Message Broker:
```csharp
var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
{
    cfg.Host("localhost", "/", h =>
    {
        h.Username("guest");
        h.Password("guest");
    });
});
```
if you are using dokcer-compsoe from MyVod project to run rabbitmq then credentials will be rabbitmq:rabbitmq
- [ ] To connect You will need to run `await busControl.StartAsync();` as well
- [ ] Add Logic to read value from Console line and publish it with busControl object like
```csharp
await busControl.Publish<ValueEntered>(new
{
    Value = value
});
```
- [ ] You can add special case for "quit" string to close the application